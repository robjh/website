-*- title: STM8-Breakout (20w23a) -*-
STM8-Breakout (20w23a)
======================

You can find the kicad files here; <https://gitlab.com/robjh/stm8-breakout>

This project is a breakout board for the STM8S003K3. It's my first foray into electrical engineering.  
If you're an expert looking over my designs, thinking to yourself; "They clearly don't know what they're doing." - You're right.  
If you're a beginner, looking for hints that might be helpful for your own projects; Just bare in mind that by virtue of being unaware of the best practices, I've not followed best practices.  
The microprocessor was chosen because it was the cheapest I could find. It is part of ST's value line, and it appears to support everything I need.
The use cases I have are similar to what you would use an STM8-discovery board for. That means evaluating the chip and just trying things out.

[![STM8-Breakout PCB](20w23a-pcb-small.jpeg)](20w23a-pcb.jpeg)

When using the stm8-discovery board, I had it sat next to a breadboard that had the components I was working with.
Moving the whole contraption was usually difficult.
This thing's form factor is meant to sit inside a breadboard, so I'm hoping my projects using an stm8 might get a bit tidier going forward.

The board features 5V input, either from the USB-micro plug or the programmer header. A voltage regulator to supply 3.3v. An external 16Mhz crystal. And an RGB LED.
I've chosen to use resistors and capacitors with a 0402 form factor. These parts are quite small. Hand soldering them was difficult, but I figured it out eventually.

[![STM8-Breakout Fully Assembled PCB](20w23a-assembled-small.jpeg)](20w23a-assembled.jpeg)

Flashing
--------

I've created a simple test program that can be downloaded from gitlab; <https://gitlab.com/robjh/stm8-testprog>  
It has the board's LED change different colours, and outputs the colour to serial.

The program requires sdcc to build (which is in most distro's repos), and stm8flash to flash onto the device (sources available on github: <https://github.com/vdudouyt/stm8flash>).
The provided Makefile assumes you're using a stlinkv2 to flash the device. If that's not the case, then the makefile will have to be modified.

	make  
	make flash

![An animation showing the breakout board's blinking LED](flashing.gif)

By connecting a UART to pin D5 and running minicom at baud 9600, you should see this sort of output;

	black  
	red  
	green  
	yellow  
	blue
	magenta  
	cyan  
	white  
