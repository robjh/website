"use strict";
(function(rob) {

	window.g = window.g || {};
	let g = window.g;

	let presentation = (function(argv, p) {
		argv     = argv || {};
		p        = p    || {};
		let self = {
			presenting: false
		};

		// Create an initally disabled stylesheet.
		// Populate it with a rule disabling the scrollbar, and a dynamic rule to scale the slide by the correct amount.
		p.style = rob.stylesheet({
			disable: true
		});
		p.style.newrule("body { overflow: hidden }").apply();
		p.rulescale = p.style.newrule(
			"section.active .slide_body {  \
				transform: scale({});      \
				top:  {}px;                \
				left: {}px;                \
			}"
		);

		// A set of controls for a presentation.
		self.present = (function(slide = 0) {
			p.sectionactive = slide;
			p.section[slide].classList.add("active");
			p.style.enable();
			p.recalc_scale(document.documentElement.clientWidth,document.documentElement.clientHeight);
			document.addEventListener("keydown", p.onkeypress);
		});
		self.stop = (function() {
			if (p.sectionactive == undefined) {
				console.error("Not currently presenting.");
				return;
			}
			p.section[p.sectionactive].classList.remove("active");
			delete(p.sectionactive);
			p.style.disable();
			document.removeEventListener("keydown", p.onkeypress);
		});
		self.prev = (function() {
			if (p.sectionactive == undefined) {
				console.error("Not currently presenting.");
				return;
			}
			if (p.sectionactive == 0) {
				console.error("Already on first slide.");
				return;
			}
			p.section[p.sectionactive].classList.remove("active");
			--p.sectionactive;
			p.section[p.sectionactive].classList.add("active");
		});
		self.next = (function() {
			if (p.sectionactive == undefined) {
				console.error("Not currently presenting.");
				return;
			}
			if (p.sectionactive >= p.section.length - 1) {
				console.error("Already on last slide.");
				return;
			}
			p.section[p.sectionactive].classList.remove("active");
			++p.sectionactive;
			p.section[p.sectionactive].classList.add("active");
		});

		// Callback functions, Nostly for on-screen buttons, but also for keyboard control and page resizing.
		p.btn_maximise_callback = (function(section_id) {
			return (function() {
				self.present(section_id);
			});
		});
		p.btn_close_callback = self.stop;
		p.btn_prev_callback  = self.prev;
		p.btn_next_callback  = self.next;

		p.onkeypress = (function(event) {
			switch (event.key) {
				case "Escape":
					self.stop();
					break;
				case "ArrowLeft":
				case "h":
					self.prev();
					break;
				case "ArrowRight":
				case "l":
				case " ":
					self.next();
					break;
			}
		});

		p.recalc_scale = (function(window_x, window_y) {
			if (p.sectionactive == undefined) return;
			let sizebox = p.section[p.sectionactive].getElementsByClassName("slide_sizebox")[0];
			let scale = (window_x / sizebox.clientWidth);
			if (sizebox.clientHeight * scale <= window_y) {
				let pad = (window_y - sizebox.clientHeight * scale) / 2;
				p.rulescale.apply(scale, pad, 0);
			} else {
				scale = (window_y / sizebox.clientHeight);
				let pad = (window_x - sizebox.clientWidth * scale) / 2;
				p.rulescale.apply(scale, 0, pad);
			}
		});
		p.token_resize = g.page.callback_resize_register(p.recalc_scale);

		// adds the on-screen buttons to each slide, and enables their callbacks.
		p.prepare_section = (function(sec_id) {
			let slide = p.section[sec_id].getElementsByClassName("slide_body")[0];
			ao.dom_apply(slide, {appendChild: [

				// maximise button
				ao.dom_node("div", {
					class: ["slide_btn", "slide_btn_maximise"],
					appendChild: ao.dom_node("img", {
						src: window.g.chroot + "/usr/share/icons/maximise.svg",
						alt: "maximise"
					}),
					onclick: p.btn_maximise_callback(sec_id)
				}),

				// close button
				ao.dom_node("div", {
					class: ["slide_btn", "slide_btn_close"],
					appendChild: ao.dom_node("img", {
						src: g.chroot + "/usr/share/icons/close.svg",
						alt: "close"
					}),
					onclick: p.btn_close_callback
				}),

				// prev button
				ao.dom_node("div", {
					class: ["slide_btn", "slide_btn_prev"],
					appendChild: ao.dom_node("img", {
						src: g.chroot + "/usr/share/icons/prev.svg",
						alt: "previous"
					}),
					onclick: p.btn_prev_callback
				}),

				// next button
				ao.dom_node("div", {
					class: ["slide_btn", "slide_btn_next"],
					appendChild: ao.dom_node("img", {
						src: g.chroot + "/usr/share/icons/next.svg",
						alt: "next"
					}),
					onclick: p.btn_next_callback
				}),

			]});
		});

		p.section = document.getElementsByTagName("section");
		for (let i = 0, l = p.section.length ; i < l ; ++i) {
			p.prepare_section(i);
		}

		// the classes destructor. Removes any registered callbacks and removes the stylesheet from the DOM.
		p.token_destructor = g.page.callback_page_unload_register(function(node) {
			self.stop();
			g.page.callback_page_unload_remove(p.token_destructor);
			g.page.callback_resize_remove(p.token_resize);
			p.style.destruct();
			delete(node.presentation);
		});


		return self;
	});
	presentation.setup_callbacks = (function(page) {
		page.callback_page_load_register(function(node) {
			if (node.is_presentation) {
				node.presentation = presentation();
			}
		});
	});

	rob.presentation = presentation;

	if (g.page) {
		// if the global page object hasnt been constructed yet, this is called when it is.
		presentation.setup_callbacks(g.page);
		g.page.loaded_script("/lib/presentation.js");
	}

	return rob;

}(window.robjh));
