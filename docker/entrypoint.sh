#!/bin/sh

# Start a webserver, monitor the project directory for changes and update the web directory.

nginx -g 'daemon off;' &


regen()
{
	./generator.py --skel filesystem/ --webdir /var/www/localhost/html/ --url http://localhost:8080/ --redirect none &> /var/project/output
}
regen

while true; do

	inotifywait -e modify,create,delete -r /var/project && \
	regen

done
