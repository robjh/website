#!/bin/bash

# Run this from the project's root directory, and use sudo if podman/docker isnt setup to run rootless

DOCKER=podman

${DOCKER} stop demo
${DOCKER} rm demo

${DOCKER} build docker/ --tag robjhdev
${DOCKER} run -d -p 8080:80 -v .:/var/project --name demo robjhdev

