#!/bin/bash

# Run this from the project's root directory, and use sudo if podman/docker isnt setup to run rootless

DOCKER=podman

${DOCKER} stop demo
${DOCKER} rm demo

